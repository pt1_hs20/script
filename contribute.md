# Contribute to PT1 script

## Info
1. The script is written in *markdown* in `README.md`
2. There is some further reading/more in-depth explanations in the `cheatsheets` folder
3. Images are done with extra html in the markdown
   - Add Image
   ```html
   <img src="graphics/pipeline.png" style="zoom:100%"/>
   ```
4. General formatting and other things like colored text
   - See [GitLab Markdown page](https://docs.gitlab.com/ee/user/markdown.html)
4. The Latex format is [Katex](https://github.com/KaTeX/KaTeX)
5. Create an issue for ToDo's

## Compile the Markdown
Compile using pandoc, this will also create the automatic table of contents (TOC).
```bash
pandoc README.md -s --toc --highlight-style tango --metadata title="Programming Techniques for Scientific Simulations I" --katex -o sc
ript.html
```

## Useful Links

- CMake: [repository with examples and explanations](https://github.com/ttroy50/cmake-examples)
- CPU Caches [Wikipedia](https://en.wikipedia.org/wiki/CPU_cache)
